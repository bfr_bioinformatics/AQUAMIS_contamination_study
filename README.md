# AQUAMIS Contamination Study 

Supplementary Data Analysis for the AQUAMIS publication introducing [AQUAMIS](https://gitlab.com/bfr_bioinformatics/AQUAMIS).

The subfolder in [/public](/public) contains the analysis of the [FDA Contamination Dataset](https://doi.org/10.6084/m9.figshare.c.4282706.v1) by Arthur Pightling _et al_.

### AQUAMIS Reports
This includes the **AQUAMIS Reports** for the pathogen datasets:
* [_Escherichia coli_](/public/FDA_dataset_Campylobacter/assembly_report.html)
* [_Salmonella enterica_](/public/FDA_dataset_Salmonella/assembly_report.html)
* [_Listeria monocytogenes_](/public/FDA_dataset_Listeria/assembly_report.html)

as well as for a [new dataset](https://zenodo.org/record/4601406) developed for the AQUAMIS study:
* [_Campylobacter_ spp.](/public/FDA_dataset_Listeria/assembly_report.html)

### Authors
* Carlus Deneke
* Holger Brendebach
* Simon H. Tausch

### References
1. AQUAMIS: https://gitlab.com/bfr_bioinformatics/AQUAMIS
2. Pightling, A.B.P., James; Wang, Yu; Rand, Hugh; Strain, Errol. Contamination Dataset 191125. figshare, 2019.